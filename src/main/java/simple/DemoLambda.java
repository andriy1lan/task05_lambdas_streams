package simple;

public class DemoLambda {
    public static void main (String [] args) {
        AggregateInterface aiavg = (a, b, c) -> {
            int avg = (a + b + c)/3;
            return avg;
        };

        AggregateInterface aimax = (a, b, c ) -> {
            int max = 0;
            if ( a > b && a > c) max=a;
            else if ( b > a && b > c) max=b;
            else if ( c > b && c > a) max=c;
            else if ( c > a && b > a && c == b) max = c;
            else if ( c > b && a > b && c == a) max = c = a;
            else if ( a > c && b > c && b == a) max = b = a;
            else max = c = b = a;
            return max;
        };

        System.out.println("Average of three numbers: " + aiavg.mathAction(1,3,4));
        System.out.println("Max of three numbers: " + aimax.mathAction(1,4,4));
    }
}
