package simple;

@FunctionalInterface
interface AggregateInterface {
    public int mathAction (int a, int b, int c);
}