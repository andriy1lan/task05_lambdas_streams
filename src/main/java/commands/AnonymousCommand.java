package commands;

class AnonymousCommand implements Command{
    InnerCommand command;
    String name;

    public AnonymousCommand (String name) {
        this.name = name;
        command = new InnerCommand();
    }

    public void execute(){
        command.doAnonymous (name);
    }

}