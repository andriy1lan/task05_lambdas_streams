package commands;

class ReferenceCommand implements Command{
    InnerCommand command;
    Method method;
    String name;

    public ReferenceCommand (String name) {
        this.name = name;
        command = new InnerCommand();
    }

    public void execute(){
        InnerCommand ic = new InnerCommand();
        method = ic::doMethod;
        method.doo(name);
    }
}
