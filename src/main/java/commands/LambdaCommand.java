package commands;

class LambdaCommand implements Command{
    InnerCommand command;
    String name;

    public LambdaCommand (String name){
        this.name = name;
        command = new InnerCommand();
    }

    public void execute(){
        command.doLambda(name);
    }
}