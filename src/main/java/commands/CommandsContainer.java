package commands;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CommandsContainer {
    List<Command> list = new ArrayList<>();

    public void addCommand(Command command) {
      list.add(command) ;
    }
    public void printCommands() {

      for (Command c : list)   {
          c.execute();
      }
    }

    public static void main (String [] args) {

        System.out.print("Please enter the name of command and the name after the return - LAMBDA - REFERENCE - ANONYMOUS - OBJECT");
        CommandsContainer container = new CommandsContainer();
        Command com = null;
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            System.out.println("Please enter next command and name");
           String []  strings = new String[2];
           strings = sc.nextLine().trim().split(" ");
           if(strings[0].length()==0) break;
               switch (strings[0]) {
                   case "Lambda":
                       com = new LambdaCommand(strings[1]);
                       break;
                   case "Reference":
                       com = new ReferenceCommand(strings[1]);
                       break;
                   case "Anonymous":
                       com = new AnonymousCommand(strings[1]);
                       break;
                   case "Object":
                       com = new ObjectCommand(strings[1]);
                       break;
                   default:
                       System.out.println("Not correct command name");
                       continue;
               }
               container.addCommand(com);
        }

        System.out.println("Do you want to see commands execution? If so - Return");
        if(sc.hasNextLine()) container.printCommands();
        }
    }