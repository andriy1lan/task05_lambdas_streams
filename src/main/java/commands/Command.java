package commands;

@FunctionalInterface
interface Command{
    public void execute();
}
