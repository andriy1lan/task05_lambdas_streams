package commands;

class ObjectCommand implements Command {
    String name;
    InnerCommand command;

    public ObjectCommand (String name) {
        this.name = name;
        command = new InnerCommand();
    }

    public  void printName() {
        System.out.println("My name is " + name);
    }

    public void execute(){
        command.doObject(this);
    }
}
