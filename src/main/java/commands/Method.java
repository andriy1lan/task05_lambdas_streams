package commands;

@FunctionalInterface
interface Method{
    public void doo(String name);
}