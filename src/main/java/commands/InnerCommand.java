package commands;

public class InnerCommand {
    Lambda lambda;
    // String name;
    public void doLambda(String name) {
        lambda = ()-> { System.out.println("My name is " + name); };
        lambda.doo();
    }

    public void doMethod(String name) {
        System.out.println("My name is " + name);
    }


    public void doAnonymous (String name) {
        Lambda rc=new Lambda () {
            public void doo() {System.out.println("My name is " + name); } };
        rc.doo();
    }

    public void doObject (ObjectCommand command) {
        command.printName();
    }
}