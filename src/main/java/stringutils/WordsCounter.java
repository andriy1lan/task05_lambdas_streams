package stringutils;

import java.util.stream.*;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.Scanner;

public class WordsCounter {

    Stream<String> stream;
    String text = "";


    public void setText(String string) {
        text = string;}

    public void countSymbols () {
        stream = Stream.of(text);
        Map<Character, Long> mapa = stream.flatMap(line -> Arrays.stream(line.trim().split(" ")))
                .map(word -> word.replaceAll("[^a-zA-Z]", "").trim())
                .filter(word -> !word.isEmpty())
                //.flatMap(word->Arrays.stream(word.toCharArray()))
                .flatMap(word->word.chars().mapToObj(c -> (char) c))
                .filter(Character::isLowerCase)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        mapa.forEach((k, v) -> System.out.println(k + " -- " + v));
    }

    public List<String> findSordtedDistinctWords () {
        stream = Stream.of(text);
        List<String> result = stream.flatMap(line -> Arrays.stream(line.trim().split(" ")))
                .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim())
                .filter(word -> !word.isEmpty()).sorted().distinct()
                .collect(Collectors.toList());
        return result;
    }

    public Stream<String> stream() {
        stream = Stream.of(text);
        Stream<String> result = stream.flatMap(line -> Arrays.stream(line.trim().split(" ")))
                .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim())
                .filter(word -> !word.isEmpty());
        return result;
    }

    public void countWords() {
        stream = Stream.of(text);
        Map<String, Long> mapa = stream.flatMap(line -> Arrays.stream(line.trim().split(" ")))
                .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim())
                .filter(word -> !word.isEmpty())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        mapa.forEach((k, v) -> System.out.println(k + " - " + v));
    }



    public static void main (String[] args) {
        WordsCounter wc = new WordsCounter();

        String str = new String();
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            if (s.equals("")) break;
            str += " ";
            str += s;
        }

        wc.setText(str);
        System.out.println("Here is the sorted list of unique words of entered text");
        System.out.println();
        for(String s:wc.findSordtedDistinctWords()) {
            System.out.print(s + " "); }
        System.out.println();
        System.out.println("Number of unique words is: " + wc.findSordtedDistinctWords().size());
        System.out.println();
        System.out.println("Here is the frequencies of every distinct word");
        wc.countWords();
        System.out.println();
        System.out.println("Then there is the frequencies of every distinct characters except Upper Case characters");
        wc.countSymbols ();
        //System.out.println(str);
    }

}

